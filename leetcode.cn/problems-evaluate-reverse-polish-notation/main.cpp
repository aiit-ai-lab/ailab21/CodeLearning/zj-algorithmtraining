class Solution
{
public:
    int evalRPN(vector<string> &tokens)
    {
        stack<int> stk;
        for (int i = 0; i < tokens.size(); i++)
        {
            if (tokens[i] == "+" || tokens[i] == "-" || tokens[i] == "*" || tokens[i] == "/")
            {
                int b = stk.top();
                stk.pop();
                int a = stk.top();
                stk.pop();
                int res = deal(a, b, tokens[i]);
                stk.push(res);
            }
            else
            {
                stk.push(stoi(tokens[i]));
            }
        }
        return stk.top();
    }
    int deal(int a, int b, string s)
    {
        char t = s[0];
        int res;
        switch (t)
        {
        case '+':
            res = a + b;
            break;
        case '-':
            res = a - b;
            break;
        case '*':
            res = (long)a * (long)b;
            break;
        case '/':
            res = a / b;
            break;
        }
        return res;
    }
};