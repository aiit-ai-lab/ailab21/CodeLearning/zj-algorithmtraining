/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution
{
public:
    vector<vector<int>> levelOrder(TreeNode *root)
    {
        queue<TreeNode *> que;
        vector<vector<int>> res;
        if (root != NULL)
        {
            que.push(root);
        }
        while (!que.empty())
        {
            vector<int> tmp;
            int size = que.size();
            for (int i = 0; i < size; i++)
            {
                TreeNode *cur = que.front();
                que.pop();
                tmp.push_back(cur->val);
                if (cur->left)
                {
                    que.push(cur->left);
                }
                if (cur->right)
                {
                    que.push(cur->right);
                }
            }
            res.push_back(tmp);
        }
        return res;
    }
};

/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class SolutionN
{
public:
    vector<vector<int>> levelOrder(Node *root)
    {
        vector<vector<int>> res;
        queue<Node *> que;
        if (root != NULL)
            que.push(root);
        while (!que.empty())
        {
            vector<int> tmp;
            int size = que.size();
            for (int i = 0; i < size; i++)
            {
                Node *cur = que.front();
                que.pop();
                tmp.push_back(cur->val);
                int cnt = (cur->children).size();
                for (int i = 0; i < cnt; i++)
                {
                    Node *temp = (cur->children)[i];
                    que.push(temp);
                }
            }
            res.push_back(tmp);
        }
        return res;
    }
};