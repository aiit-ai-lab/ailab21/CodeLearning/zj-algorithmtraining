class Solution
{
public:
    int totalFruit(vector<int> &fruits)
    {
        int res = 0, left = 0, right = 0;
        int fruita = fruits[left], fruitb = fruits[right];
        while (right < fruits.size())
        {
            if (fruits[right] == fruita || fruits[right] == fruitb)
            {
                res = max(res, right - left + 1);
                right++;
            }
            else
            {
                left = right - 1;
                fruita = fruits[left];
                while (left >= 1 && fruits[left - 1] == fruita)
                {
                    left--;
                }
                fruitb = fruits[right];
                res = max(res, right - left + 1);
                right++;
            }
        }
        return res;
    }
};