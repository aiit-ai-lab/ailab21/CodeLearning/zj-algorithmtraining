class Solution
{
public:
    vector<int> goodIndices(vector<int> &nums, int k)
    {
        int n = nums.size();
        vector<int> ans;
        vector<bool> pos(n, false);
        for (int i = n - 2, j = 1; i >= 0; i--)
        {
            if (j >= k)
            {
                pos[i] = true;
            }
            if (nums[i] <= nums[i + 1])
            {
                j++;
            }
            else
            {
                j = 1;
            }
        }
        for (int i = 1, j = 1; i < n; i++)
        {
            if (j >= k && pos[i])
            {
                ans.push_back(i);
            }
            if (nums[i] <= nums[i - 1])
            {
                j++;
            }
            else
            {
                j = 1;
            }
        }
        return ans;
    }
};