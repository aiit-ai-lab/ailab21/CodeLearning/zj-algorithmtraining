/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() : val(0), left(NULL), right(NULL), next(NULL) {}

    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};
*/

class Solution
{
public:
    Node *connect(Node *root)
    {
        Node *mark = root;
        queue<Node *> que;
        if (root != NULL)
            que.push(root);
        while (!que.empty())
        {
            int size = que.size();
            Node *pre = NULL;
            Node *cur;
            for (int i = 0; i < size; i++)
            {
                cur = que.front();
                que.pop();
                cur->next = pre;
                pre = cur;
                if (cur->right)
                    que.push(cur->right);
                if (cur->left)
                    que.push(cur->left);
            }
            /*官方写法（更推荐）
            for(int i = 0; i < size; i++) {

                // 从队首取出元素
                Node* node = Q.front();
                Q.pop();

                // 连接
                if (i < size - 1) {
                    node->next = Q.front();
                }

                // 拓展下一层节点
                if (node->left != nullptr) {
                    Q.push(node->left);
                }
                if (node->right != nullptr) {
                    Q.push(node->right);
                }
            }
            */
        }
        return root;
    }
};