class Solution
{
public:
    string replaceSpace(string s)
    {
        //法1
        int count = 0;
        for (int i = 0; i < s.size(); i++)
        {
            if (s[i] == ' ')
                count++;
        }
        int i = s.size() - 1;
        s.resize(s.size() + count * 2);
        for (int j = s.size() - 1; i >= 0; i--)
        {
            if (s[i] != ' ')
            {
                s[j--] = s[i];
            }
            else
            {
                s[j--] = '0';
                s[j--] = '2';
                s[j--] = '%';
            }
        }
        return s;
        /*
        法二
        class Solution {
        public:
            string replaceSpace(string s) {
                string tmp;
                for(int i = 0; i < s.size(); i++){
                    if(s[i] != ' '){
                        tmp.push_back(s[i]);
                    }
                    else{
                        tmp.append("%20");
                    }
                }
                return tmp;
            }
        };
        */
    }
};