class Solution
{
public:
    bool backspaceCompare(string s, string t)
    {
        return rebuild(s) == rebuild(t);
    }
    //法一
    string rebuild(string &s)
    {
        string tmp;
        for (int i = 0; i < s.size(); i++)
        {
            if (s[i] != '#')
            {
                tmp.push_back(s[i]);
            }
            else if (!tmp.empty())
            {
                tmp.pop_back();
            }
        }
        return tmp;
    }
    /*
    法二
    class Solution {
    public:
        bool backspaceCompare(string s, string t) {
            int i = s.size(), j = t.size();
            int skips = 0, skipt = 0;
            while(i >= 0 || j >= 0){
                while(i >= 0){
                    if(s[i] == '#'){
                        skips++;
                        i--;
                    }
                    else if(skips > 0){
                        skips--;
                        i--;
                    }
                    else{
                        break;
                    }
                }
                while(j >= 0){
                    if(t[j] == '#'){
                        skipt++;
                        j--;
                    }
                    else if(skipt > 0){
                        skipt--;
                        j--;
                    }
                    else
                        break;
                }
                if(i >= 0 && j >= 0){
                    if(s[i] != t[j]){
                        return false;
                    }
                }
                else if(i >= 0 || j >= 0){
                    return false;
                }
                i--;
                j--;
            }
            return true;
        }
    };

    */
};