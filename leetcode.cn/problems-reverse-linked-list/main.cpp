/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution
{
public:
    ListNode *reverseList(ListNode *head)
    {
        //二次
        ListNode *slow = NULL;
        ListNode *fast = head;
        ListNode *tmp;
        while (fast)
        {
            tmp = fast->next;
            fast->next = slow;
            slow = fast;
            fast = tmp;
        }
        return slow;
        /*
        一次
        if(!head || !head->next)
            return head;
        ListNode *p, *q;
        p = head;
        q = NULL;
        while(p){
            ListNode *tmp = p->next;
            p->next = q;
            q = p;
            p = tmp;
        }
        return q;
        */
    }
};