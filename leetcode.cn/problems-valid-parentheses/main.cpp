class Solution
{
public:
    /*  法一
    unordered_map<char, char> pairs = {
        {')', '('},
        {']', '['},
        {'}', '{'}};
    bool isValid(string s)
    {
        stack<char> st;
        int n = s.size();
        if (n % 2)
            return false;
        for (int i = 0; i < n; i++)
        {
            if (s[i] == ')' || s[i] == '}' || s[i] == ']')
            {
                if (st.empty() || st.top() != pairs[s[i]])
                    return false;
                st.pop();
            }
            else
            {
                st.push(s[i]);
            }
        }
        return st.empty();
    }*/
    //法二
    class Solution
    {
    public:
        bool isValid(string s)
        {
            stack<char> st;
            int n = s.size();
            if (n % 2)
                return false;
            for (int i = 0; i < n; i++)
            {
                if (s[i] == '(')
                    st.push(')');
                else if (s[i] == '{')
                    st.push('}');
                else if (s[i] == '[')
                    st.push(']');
                else
                {
                    if (st.empty() || st.top() != s[i])
                        return false;
                    st.pop();
                }
            }
            return st.empty();
        }
    };
};