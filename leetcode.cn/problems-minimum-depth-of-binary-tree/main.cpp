/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution
{
public:
    //广度搜索法
    int minDepth(TreeNode *root)
    {
        if (root == NULL)
        {
            return 0;
        }
        queue<pair<TreeNode *, int>> que;
        que.emplace(root, 1);
        while (!que.empty())
        {
            TreeNode *cur = que.front().first;
            int depth = que.front().second;
            que.pop();
            if (cur->left == NULL && cur->right == NULL)
            {
                return depth;
            }
            if (cur->left)
            {
                que.emplace(cur->left, depth + 1);
            }
            if (cur->right)
            {
                que.emplace(cur->right, depth + 1);
            }
        }
        return 0;
    }

    //深度搜索法
    int minDepth(TreeNode *root)
    {
        if (root == NULL)
        {
            return 0;
        }
        if (root->left == NULL && root->right == NULL)
        {
            return 1;
        }
        int mindepth = INT_MAX;
        if (root->left)
        {
            mindepth = min(minDepth(root->left), mindepth);
        }
        if (root->right)
        {
            mindepth = min(minDepth(root->right), mindepth);
        }
        return mindepth + 1;
    }
};