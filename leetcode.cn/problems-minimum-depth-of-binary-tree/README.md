# 二叉树最小深度
## 广度搜索法
利用队列结构，不断遍历每一层的结点并做判断，找到第一个叶子结点直接返回结果。

## 深度搜索法
参考求最大深度的深度搜索法