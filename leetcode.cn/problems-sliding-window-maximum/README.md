# 滑动窗口最大值
## 单调队列
自定义单调队列函数（从大到小）。

先处理第一个窗口，再步步将窗口后移。

**自定义pop、push函数**
当窗口后移时，新元素进入窗口，在push函数中，将所有比其小的元素都pop_back()出去，又因为自定义的pop函数，使得之前窗口中的元素不会积压，避免了错误。

通过两个函数使得队列中只存在当前窗口中较大的数，且从大到小排列。