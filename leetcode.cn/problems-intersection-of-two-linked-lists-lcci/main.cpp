/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution
{
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB)
    {
        ListNode *curA, *curB;
        curA = headA;
        curB = headB;
        int countA = 0;
        int countB = 0;
        while (curA)
        {
            countA++;
            curA = curA->next;
        }
        while (curB)
        {
            countB++;
            curB = curB->next;
        }
        curA = headA;
        curB = headB;
        if (countB > countA)
        {
            swap(curA, curB);
            swap(countA, countB);
        }
        int x = countA - countB;
        while (x--)
        {
            curA = curA->next;
        }
        while (curA)
        {
            if (curA == curB)
                return curA;
            curA = curA->next;
            curB = curB->next;
        }
        return NULL;
    }
};