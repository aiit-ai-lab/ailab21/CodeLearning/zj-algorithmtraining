class Solution
{
public:
    int removeDuplicates(vector<int> &nums)
    {
        int n = nums.size();
        /*
        若题目未说明 1 <= nums.length <= 3 * 10的4次方，则需加上判断
        if(n == 0)
            return 0;
        */
        int slowptr = 0;
        for (int fastptr = 1; fastptr < n; fastptr++)
        {
            if (nums[fastptr] != nums[slowptr])
            {
                nums[++slowptr] = nums[fastptr];
            }
        }
        return (slowptr + 1);
    }
};