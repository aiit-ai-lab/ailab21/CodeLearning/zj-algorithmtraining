/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution
{
public:
    ListNode *removeNthFromEnd(ListNode *head, int n)
    {
        ListNode *fhead = new ListNode;
        fhead->next = head;
        ListNode *pre = fhead;
        ListNode *cur = fhead;
        while (n-- > 0)
            pre = pre->next;
        while (pre->next)
        {
            cur = cur->next;
            pre = pre->next;
        }
        ListNode *tmp = cur->next;
        cur->next = tmp->next;
        delete tmp;
        return fhead->next;
    }
};