class Solution
{
public:
    int longestContinuousSubstring(string s)
    {
        int n = s.size();
        int res = 0;
        for (int j = 0, i = 1; i <= n; i++)
        {
            if (i == n || s[i] != s[i - 1] + 1)
            {
                res = max(res, i - j);
                j = i;
            }
        }
        return res;
    }
};