#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100010;
int q[N], p[N];
int main()
{
    int n, b;
    cin >> n >> b;
    int flag;
    for (int i = 1; i <= n; i++)
    {
        cin >> q[i];
        if (q[i] > b)
            q[i] = 1;
        else if (q[i] < b)
            q[i] = -1;
        else
            flag = i;
    }
    int sum = 0;
    int ans = 1;
    for (int i = flag - 1; i >= 1; i--)
    {
        sum += q[i];
        p[n + sum]++;
        if (sum == 0)
            ans++;
    }
    sum = 0;
    for (int i = flag + 1; i <= n; i++)
    {
        sum += q[i];
        ans += p[n - sum];
        if (sum == 0)
            ans++;
    }
    cout << ans << endl;
    return 0;
}