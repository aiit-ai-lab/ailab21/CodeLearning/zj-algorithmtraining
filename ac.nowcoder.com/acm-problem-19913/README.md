# 中位数图
## 前缀和与差分
大于目标数置为1，小于则置为0，以目标数位置为界，向左扩散计算前缀和并同时对右做标记，然后向右扩散计算前缀和，注意是否满足左边的标记。