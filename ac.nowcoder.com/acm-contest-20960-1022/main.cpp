#include <iostream>
#include <algorithm>
using namespace std;
typedef long long ll;
ll s[2000005];
int main()
{
    int T;
    cin >> T;
    while (T--)
    {
        int n, k;
        cin >> n >> k;
        for (int i = 1; i <= n; i++)
        {
            cin >> s[i];
            s[i] += s[i - 1];
        }
        ll ma = -1e18, ans = -1e18;
        for (int i = k; i + k <= n; i++)
        {
            ma = max(ma, s[i] - s[i - k]);
            ans = max(ans, ma + s[i + k] - s[i]);
        }
        cout << ans << endl;
    }
    return 0;
}