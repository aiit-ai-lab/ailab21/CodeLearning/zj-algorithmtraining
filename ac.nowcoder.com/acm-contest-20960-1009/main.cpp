/* 忽略了16进制数
#include <iostream>
#include <vector>
using namespace std;
void add(int N, vector<int>& res){
    int n = res.size();
    vector<int> vc;
    int flag = 0;
    for (int i = 0; i < n; i++){
        int x = (res[i] + res[n - 1 - i] + flag) % N;
        int y = (res[i] + res[n - 1 - i] + flag) / N;
        vc.push_back(x);
        flag = y;
    }
    if(flag)
        vc.push_back(flag);
    res.swap(vc);
}
bool judge(vector<int> res){
    int n = res.size();
    for (int i = 0; i < n / 2; i++){
        if(res[i] != res[n - 1 - i])
            return false;
    }
    return true;
}
int main(){
    int N, M;
    cin >> N >> M;
    int step = 0;
    vector<int> res;
    int tmp = M;
    while (tmp){
        res.push_back(tmp % 10);
        tmp /= 10;
    }
    while (step < 30)
    {
        step++;
        add(N, res);
        bool flag = judge(res);
        if (flag)
        {
            cout << "STEP=" << step;
            return 0;
        }
    }
    return 0;
}*/
#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
string add(const string &s, int N)
{
    string t = s;
    reverse(t.begin(), t.end());
    string res;
    for (int i = 0; i < s.size(); i++){
        res.push_back(s[i] + t[i] - '0');
    }

    res.push_back('0');
    for (int i = 0; i < res.size(); i++){
        if (res[i] >= N + '0'){
            res[i] -= N;
            res[i + 1]++;
        }
    }
    while (res.back() == '0')
        res.pop_back();
    return res;
}
bool judge(string res)
{
    for (int i = 0; i < res.size() / 2; i++)
        if (res[i] != res[res.size() - 1 - i])
            return false;
    return true;
}
int main()
{
    int N;
    cin >> N;
    string M;
    cin >> M;
    for (char &c : M){
        switch (c){
        case 'A': c = 10 + '0'; break;
        case 'B': c = 11 + '0'; break;
        case 'C': c = 12 + '0'; break;
        case 'D': c = 13 + '0'; break;
        case 'E': c = 14 + '0'; break;
        case 'F': c = 15 + '0'; break;
        default: break;
        }
    }
    int step = 0;
    while(!judge(M)){
        if(step >= 30){
            cout << "Impossible!\n";
            return 0;
        }
        M = std::move(add(M, N));
        step++;
    }
    cout << "STEP=" << step << endl;
    return 0;
}