#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int value[105][105] = {0};
int deal(int i, int j, int n, int m)
{
    int sum = 0;
    for (int x = 0; x < n; x++)
    {
        for (int y = 0; y < m; y++)
        {
            sum = sum + (value[x][y] * (abs(x - i) + abs(y - j)));
        }
    }
    return sum;
}
int main()
{
    int T;
    cin >> T;
    int m, n;
    while (T--)
    {
        cin >> n >> m;
        int ans = 999999999;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                cin >> value[i][j];
            }
        }
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                int t = deal(i, j, n, m);
                ans = min(ans, t);
            }
        }
        cout << ans << endl;
    }
    return 0;
}