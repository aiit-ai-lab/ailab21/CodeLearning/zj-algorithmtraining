#include <iostream>
#include <algorithm>
using namespace std;
#define MAX 1000005
int s[MAX] = {0};
void prepare()
{ //标记质数
    for (int i = 2; i <= MAX / 2; i++)
        for (int j = 2 * i; j <= MAX; j += i)
            s[j] = 1; //不为质数
}
long long result(int N, int P)
{
    long long sum = 0;
    long long factorial = 1;
    for (int i = 2; i <= min(N, P); i++)
    {
        factorial = (factorial * i) % P;
        if (s[i] != 1)
        {
            sum += factorial;
            sum %= P;
        }
    }
    return sum;
}
int main()
{
    prepare();
    int T;
    cin >> T;
    int N, P;
    while (T--)
    {
        cin >> N >> P;
        long long int sum = result(N, P);
        cout << sum << endl;
    }
    return 0;
}