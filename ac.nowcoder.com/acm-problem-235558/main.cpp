#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100010;
int q[N] = {0};

int bsearch1(int l, int r, int x)
{
    while (l < r)
    {
        int mid = (l + r) >> 1;
        if (q[mid] >= x)
            r = mid;
        else
            l = mid + 1;
    }
    return l;
}

int bsearch2(int l, int r, int y)
{
    while (l < r)
    {
        int mid = (l + r + 1) >> 1;
        if (q[mid] <= y)
            l = mid;
        else
            r = mid - 1;
    }
    return l;
}

int main()
{
    int n, T;
    cin >> n;
    for (int i = 0; i < n; i++)
        cin >> q[i];
    int x, y;
    cin >> T;
    while (T--)
    {
        cin >> x >> y;
        int l = bsearch1(0, n - 1, x);
        int r = bsearch2(0, n - 1, y);
        cout << r - l + 1 << endl;
    }
    return 0;
}