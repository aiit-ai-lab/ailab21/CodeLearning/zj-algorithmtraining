#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
int main()
{
    string n, m;
    cin >> n >> m;
    string ans;
    reverse(n.begin(), n.end());
    reverse(m.begin(), m.end());
    int len1 = n.length();
    int len2 = m.length();
    int len = min(len1, len2);
    for (int i = 0; i < len; i++)
        ans.push_back(n[i] + m[i] - '0');
    for (int i = len; i < len1; i++)
        ans.push_back(n[i]);
    for (int i = len; i < len2; i++)
        ans.push_back(m[i]);
    ans.push_back('0');
    for (int i = 0; i < ans.length(); i++)
    {
        if (ans[i] >= 10 + '0')
        {
            ans[i] -= 10;
            ans[i + 1]++;
        }
    }
    while (ans.back() == '0')
        ans.pop_back();
    for (int i = ans.length() - 1; i >= 0; i--)
        cout << ans[i];
    return 0;
}