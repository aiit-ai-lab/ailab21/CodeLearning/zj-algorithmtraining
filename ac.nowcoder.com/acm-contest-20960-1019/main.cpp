#include <iostream>
#include <algorithm>
using namespace std;
struct Node
{
    int l, r, d;
} p[100007];
bool cmp(Node a, Node b)
{
    if (a.d == b.d)
        return a.l < b.l;
    return a.d < b.d;
}
int visit[100007];
int main()
{
    int n, m;
    cin >> n >> m;
    for (int i = 0; i < m; i++)
    {
        cin >> p[i].l >> p[i].r >> p[i].d;
    }
    sort(p, p + m, cmp);
    int l = p[0].l, r = p[0].r;
    for (int i = 1; i < m; i++)
    {
        if (p[i].d == p[i - 1].d && p[i].l <= r) //合并区间
        {
            r = max(r, p[i].r);
        }
        else
        {
            visit[l]++;
            visit[r + 1]--;
            l = p[i].l;
            r = p[i].r;
        }
    }
    visit[l]++;
    visit[r + 1]++;
    int ans = 1;
    for (int i = 2; i <= n; i++)
    {
        visit[i] += visit[i - 1];
        if (visit[i] > visit[ans])
            ans = i;
    }
    cout << ans;
    return 0;
}