class Solution
{
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param n int
     * @param m int
     * @param a intvector
     * @return int
     */
    int solve(int n, int m, vector<int> &a)
    {
        // write code here
        int ans = 0, tmp = 0;
        for (int l = 0, r = 0; r < n; r++)
        {
            if (a[r] == 0)
                tmp++;
            while (l <= r && tmp > m)
            {
                if (a[l] == 0)
                    tmp--;
                l++;
            }
            ans = max(ans, r - l + 1);
        }
        return ans;
    }
};