#include <iostream>
#include <algorithm>
using namespace std;
int s[100005] = {0};
int main()
{
    int n;
    cin >> n;
    int sum = 0;
    for (int i = 1; i <= n; i++)
    {
        cin >> s[i];
        sum += s[i];
    }
    sum /= 2;
    int l = 1, r = 1;
    int ans = -1, t = s[1];
    while (r <= n)
    {
        while (t <= sum && r <= n)
        {
            ans = max(ans, t);
            r++;
            t += s[r];
        }
        while (t > sum && l <= r)
        {
            t -= s[l++];
        }
    }
    cout << ans << endl;
    return 0;
}
