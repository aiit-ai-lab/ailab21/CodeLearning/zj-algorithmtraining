#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
int s[40][40] = {0};
int main()
{
    int N;
    cin >> N;
    s[0][N / 2] = 1;
    int x = 0;
    int y = N / 2;
    for (int i = 2; i <= N * N; i++)
    {
        if (x == 0 && y != N - 1)
        {
            s[N - 1][y + 1] = i;
            x = N - 1;
            y++;
        }
        else if (x != 0 && y == N - 1)
        {
            s[x - 1][0] = i;
            x--;
            y = 0;
        }

        else if (x == 0 && y == N - 1)
        {
            s[x + 1][y] = i;
            x++;
        }
        else if (x != 0 && y != N - 1)
        {
            if (s[x - 1][y + 1] == 0)
            {
                s[x - 1][y + 1] = i;
                x--;
                y++;
            }
            else
            {
                s[++x][y] = i;
            }
        }
    }
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
            cout << s[i][j] << " ";
        cout << endl;
    }
    return 0;
}