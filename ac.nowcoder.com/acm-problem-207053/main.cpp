#include<iostream>
#include<map>
#include<limits.h>
#include<algorithm>
using namespace std;
const int inf = INT_MAX;
map<int, int> p;
int main()
{
    int n;
    cin >> n;
    int x;
    char c;
    for (int i = 0; i < n; i++) {
        cin >> x >> c;
        if(c == '.') {
            p[x]++;
            p[x + 1]--;
        }
        else if(c == '+') {
            p[-inf]++;
            p[x]--;
        }
        else
            p[x + 1]++;
    }
    int sum = 0, cnt = 0;
    for(auto x : p) {
        sum += x.second;
        cnt = max(cnt, sum);
    }
    cout << cnt;
    return 0;
}