#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;
typedef long long ll;
#define MOD 1000000007
ll dist[200005];
ll num[200005];
ll cost[200005];
int main()
{
    int n, m;
    cin >> n >> m;
    dist[1] = 0;
    num[0] = 0;
    cost[0] = 0;
    for (int i = 2; i <= n; i++)
    {
        cin >> dist[i];
        dist[i] = (dist[i] + dist[i - 1]) % MOD;    //dist[i]表示从第1个储物点到第i个储物点的距离（模MOD）
    }
    for (int i = 1; i <= n; i++)
    {
        cin >> num[i];
        cost[i] = (cost[i - 1] + (num[i] * dist[i]) % MOD) % MOD;   //cost[i]表示把第1-i个仓库的物品运至第1个仓库的花费
        num[i] = (num[i] + num[i - 1]) % MOD;   //num[i]表示第1-i个仓库物品总数
    }
    int tmp = m;
    int x, l, r;
    while (tmp--)
    {
        cin >> x >> l >> r;
        ll res = 0;
        if(x == l && l == r)
            cout << res << endl;
        else {
            if(x < l){
                //把l-r仓库物品总和挪至仓库1 减去 把这些物品从x挪至1的花费
                res = (((cost[r] - cost[l - 1]) - (dist[x] * (num[r] - num[l - 1])) % MOD) % MOD + MOD) % MOD;
            }
            else if(x > r) {
                //把这些物品从x挪至1的花费 - （把l-r仓库物品总和挪至仓库1）
                res = (((dist[x] * (num[r] - num[l - 1])) % MOD - (cost[r] - cost[l - 1])) % MOD + MOD) % MOD;
            }
            else {
                res = (((dist[x] * (num[x] - num[l - 1])) % MOD - (cost[x] - cost[l - 1])) % MOD + MOD) % MOD;
                res = ((res + (cost[r] - cost[x]) - (dist[x] * (num[r] - num[x])) % MOD) % MOD + MOD) % MOD;
            }
            cout << res << endl;
        }
    }
    return 0;
}