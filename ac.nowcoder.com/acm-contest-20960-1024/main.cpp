#include <iostream>
#include <algorithm>
using namespace std;
int s[5005][5005] = {0};
int main()
{
    int t, R;
    cin >> t >> R;
    int x, y, v;
    int n, m;
    n = m = R;
    for (int i = 0; i < t; i++)
    {
        cin >> x >> y >> v;
        ++x;
        ++y;
        n = max(n, x);
        m = max(y, m);
        s[x][y] = v;
    }
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            s[i][j] += s[i - 1][j] + s[i][j - 1] - s[i - 1][j - 1];
        }
    }
    int ans = 0;
    for (int i = R; i <= n; i++)
    {
        for (int j = R; j <= m; j++)
        {
            int sum = s[i][j] - s[i - R][j] - s[i][j - R] + s[i - R][j - R];
            ans = max(ans, sum);
        }
    }
    cout << ans;
    return 0;
}