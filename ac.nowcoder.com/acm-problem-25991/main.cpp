#include <iostream>
#include <algorithm>
using namespace std;
struct Node
{
    int val;
    struct Node *next;
} Node;
typedef struct Node *LinkList;
void Init(LinkList &s)
{
    s = (LinkList)malloc(sizeof(Node));
    s->next = NULL;
}

void Create(LinkList &s)
{
    LinkList r = s;
    for (int i = 0; i < 15; i++)
    {
        LinkList tmp = (LinkList)malloc(sizeof(Node));
        cin >> tmp->val;
        r->next = tmp;
        r = tmp;
    }
    r->next = NULL;
}

void Traverse(LinkList s)
{
    LinkList tmp = s->next;
    while (tmp)
    {
        cout << tmp->val << " ";
        tmp = tmp->next;
    }
    cout << endl;
}

LinkList Merge(LinkList a, LinkList b)
{
    LinkList ans;
    Init(ans);
    LinkList r = ans;
    LinkList x = a->next, y = b->next;
    while (x && y)
    {
        if (x->val <= y->val)
        {
            r->next = x;
            r = x;
            x = x->next;
        }
        else
        {
            r->next = y;
            r = y;
            y = y->next;
        }
    }
    while (x)
    {
        r->next = x;
        r = x;
        x = x->next;
    }
    while (y)
    {
        r->next = y;
        r = y;
        y = y->next;
    }
    r->next = NULL;
    return ans;
}

int main()
{
    LinkList a, b;
    Init(a);
    Init(b);
    Create(a);
    Create(b);
    LinkList ans = Merge(a, b);
    Traverse(ans);
    return 0;
}