#include <iostream>
#include <algorithm>
#include <string.h>
using namespace std;
#define MAX 100000005
int L[MAX] = {0};
int main()
{
    int l, M;
    cin >> l >> M;
    int ans = 0;
    int x, y;
    int sum = 0; //记录变量
    for (int i = 1; i <= M; i++)
    {
        cin >> x >> y;
        L[x]++;
        L[y + 1]--;
    }
    for (int i = 0; i <= l; i++)
    {
        sum += L[i];
        if (sum == 0) //此时标志空区间结束
            ans++;
    }
    cout << ans;
    return 0;
}
/*#include <iostream>
#include <algorithm>
using namespace std;
struct Node
{
    int l, r;
} p[1000005];
int visit[100000000];
bool cmp(Node a, Node b)
{
    if (a.l == b.l)
        return a.r < b.r;
    return a.l < b.l;
}
int main()
{
    int L, M;
    cin >> L >> M;
    for (int i = 0; i < M; i++)
    {
        cin >> p[i].l >> p[i].r;
    }
    sort(p, p + M, cmp);
    for (int i = 0; i < M; i++)
    {
        cout << p[i].l << " " << p[i].r << endl;
    }
    int l = p[0].l, r = p[0].r;
    for (int i = 1; i < M; i++)
    {
        if (p[i].l <= r)
        {
            r = max(r, p[i].r);
        }
        else
        {
            visit[l]++;
            visit[r + 1]--;
            l = p[i].l;
            r = p[i].r;
        }
    }
    visit[l]++;
    visit[r + 1]--;
    int sum = 0;
    int ans = 0;
    for (int i = 0; i <= L; i++)
    {
        sum += visit[i];
        if (sum == 0)
            ans++;
    }
    cout << ans;
    return 0;
}*/