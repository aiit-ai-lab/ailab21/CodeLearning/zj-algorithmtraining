#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
int main()
{
    int r[27] = {0};
    string s;
    cin >> s;
    int size = s.length();
    int res = 10000000;
    int cnt = 0;
    for (int i = 0, j = 0; j < size; j++)
    {
        if ((r[s[j] - 97]) == 0)
        {
            cnt++;
        }
        r[s[j] - 97]++;
        while (r[s[i] - 97] > 1)
        {
            r[s[i] - 97]--;
            i++;
        }
        if (cnt == 26)
            res = min(res, j - i + 1);
    }
    cout << res;
    return 0;
}