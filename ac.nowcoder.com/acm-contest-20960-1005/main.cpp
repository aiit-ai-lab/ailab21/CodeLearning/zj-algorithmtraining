#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;
int main()
{
    int r[4][4];
    string s;
    while (cin >> s)
    {
        memset(r, 0, sizeof(r));
        int size = s.length();
        for (int i = 0; i < size; i++)
        {
            if (i == size - 1)
            {
                cout << "YES" << endl;
                break;
            }
            int x = s[i] - '0';
            int y = s[i + 1] - '0';
            int m, n, p, q;
            if (x % 3 == 0)
            {
                m = x / 3;
                n = 3;
            }
            else
            {
                m = x / 3 + 1;
                n = x % 3;
            }
            if (y % 3 == 0)
            {
                p = y / 3;
                q = 3;
            }
            else
            {
                p = y / 3 + 1;
                q = y % 3;
            }
            r[m][n] = 1;
            if (r[p][q] != 0)
            { //数字重复（与每次对下一位数字做处理有关）
                cout << "NO" << endl;
                break;
            }
            if ((m + p) % 2 == 0 && (n + q) % 2 == 0)
            {
                r[p][q] = 1; //直接对下一位做处理
                if (r[(m + p) / 2][(n + q) / 2] == 0)
                { //两数字之间结点未被使用
                    cout << "NO" << endl;
                    break;
                }
            }
            else
            {
                r[p][q] = 1;
            }
        }
    }
}