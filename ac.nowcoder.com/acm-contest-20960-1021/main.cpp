#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long ll;
struct Node
{
    int a;
    ll b;
} S[50005];
int C[50005];
int main()
{
    int T;
    cin >> T;
    int tmp;
    while (T--)
    {
        memset(C, 0, sizeof(C));
        int n, m;
        cin >> n >> m;
        for (int i = 1; i <= n; i++)
        {
            cin >> S[i].a >> S[i].b;
        }
        for (int i = 1; i <= m; i++)
        {
            cin >> tmp;
            C[tmp]++;
        }
        for (int i = n; i >= 1; i--)
        {
            C[i] += C[i + 1];
            S[i].b += C[i];
        }
        int count = 0;
        ll h0 = 0, h1 = 0;
        for (int i = n; i >= 1; i--)
        {
            if (S[i].a == 0)
            {
                h0 = max(h0, S[i].b);
                if (S[i].b < h1)
                    count++;
            }
            else
            {
                h1 = max(h1, S[i].b);
                if (S[i].b < h0)
                    count++;
            }
        }
        cout << n - count << endl;
    }
    return 0;
}