#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;
struct Toy
{
    int sign;
    string name;
};
int main()
{
    vector<Toy> toys;
    int n, m;
    cin >> n >> m;
    for (int i = 0; i < n; i++)
    {
        Toy tmp;
        cin >> tmp.sign >> tmp.name;
        toys.push_back(tmp);
    }
    int len = toys.size();
    int ai, si;
    int mark = 0;
    for (int i = 0; i < m; i++)
    {
        cin >> ai >> si;
        if (ai == 0)
        {
            if (toys[mark].sign == 0)
            {
                mark = (mark + len - si) % len;
            }
            else
            {
                mark = (mark + si) % len;
            }
        }
        else if (ai == 1)
        {
            if (toys[mark].sign == 0)
            {
                mark = (mark + si) % len;
            }
            else
            {
                mark = (mark + len - si) % len;
            }
        }
    }
    cout << toys[mark].name;
    return 0;
}
